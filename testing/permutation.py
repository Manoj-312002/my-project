from math import factorial
def perm(string):
    k = 0
    t = len(string)
    res = [string]
    def permutation(rest):
        nonlocal k
        if k >= t- 1:
            return rest
        else:
            w = []
            for j in rest:
                for i in range(1,t-k):
                    a = i+k
                    w.append(j[:k]+j[a:]+j[k:a])
                
            k +=1
            rest.append(w)    
            permutation(rest)
    permutation(res)
    return res


print(perm('spam'))

