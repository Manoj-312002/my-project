k = 0
t = 0
def permutation(rest):

    global k,t
    if k == 0: t = len(*rest) 

    if k < t- 1:
        w = []
        for j in rest:
            for i in range(1,t-k):
                a = i+k
                w.append(j[:k]+j[a:]+j[k:a])
                
        k +=1
        rest += w    
        permutation(rest)

    return rest


def permutation1(seq):
    if not seq: return [seq]
    else :
        res = []
        for i in range(len(seq)):
            rest = seq[:i] + seq[i+1:]
            for x in permutation1(rest):
                res.append(seq[i:i+1] +x)
        return res

def permutation2(string):
    k = 0
    t = len(string)
    res = [string]
    def permutation(rest):
        nonlocal k
        if k >= t- 1:
            return rest
        else:
            w = []
            for j in rest:
                for i in range(1,t-k):
                    a = i+k
                    w.append(j[:k]+j[a:]+j[k:a])
                
            k +=1
            rest += w 
            permutation(rest)
    permutation(res)
    return res

def permute(a, l, r): 
    if l==r: 
        yield a 
    else: 
        for i in range(l,r+1): 
            a[l], a[i] = a[i], a[l] 
            permute(a, l+1, r) 
            a[l], a[i] = a[i], a[l] # backtrack 


import time

value = 'spam'

fi = time.perf_counter()
print(permutation([value]))
se = time.perf_counter()
print(permutation1(value))
th = time.perf_counter()
print(permutation2(value))
fo = time.perf_counter()
n = len(value) 
a = list(value) 
for i in permute(a, 0, n-1) :
    print(i)
fiv = time.perf_counter()

import sys
print(sys.getsizeof(permute))
print(sys.getsizeof(permutation1))
print(sys.getsizeof(permutation2))
print(sys.getsizeof(permutation))
 
print(se -fi , th -se ,fo-th ,fiv-fo)